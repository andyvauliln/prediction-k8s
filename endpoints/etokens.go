package endpoints

import (
	"net/http"

	"github.com/ethereum/go-ethereum/common"

	"gitlab.com/andyvauliln/prediction-k8s/interfaces"
	"gitlab.com/andyvauliln/prediction-k8s/utils/httputils"

	"github.com/gorilla/mux"
)

type etokenEndpoint struct {
	etokenService interfaces.EtokenService
}

// ServeEtokenResource sets up the routing of etokens endpoints and the corresponding handlers.
func ServeEtokenResource(
	r *mux.Router,
	p interfaces.EtokenService,
) {
	e := &etokenEndpoint{p}
	r.HandleFunc("/etoken/{address}", e.HandleGetEtoken).Methods("GET")
	r.HandleFunc("/etokens", e.HandleGetAllEtokens).Methods("GET")
}

func (e *etokenEndpoint) HandleGetAllEtokens(w http.ResponseWriter, r *http.Request) {
	res, err := e.etokenService.GetAll()
	if err != nil {
		logger.Error(err.Error())
		httputils.WriteError(w, http.StatusInternalServerError, "")
		return
	}

	httputils.WriteJSON(w, http.StatusOK, res)
}

func (e *etokenEndpoint) HandleGetEtoken(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	Address := vars["address"]

	if !common.IsHexAddress(Address) {
		httputils.WriteError(w, http.StatusBadRequest, "Invalid Address")
	}

	AddressHex := common.HexToAddress(Address)

	res, err := e.etokenService.GetByEtokenAddress(AddressHex)

	if err != nil {
		logger.Error(err.Error())
		httputils.WriteError(w, http.StatusInternalServerError, "")
		return
	}

	httputils.WriteJSON(w, http.StatusOK, res)
}
