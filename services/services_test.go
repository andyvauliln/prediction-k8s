package services

import (
	"io/ioutil"

	"gitlab.com/andyvauliln/prediction-k8s/daos"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/dbtest"
)

var server dbtest.DBServer
var db *mgo.Session

func init() {
	temp, _ := ioutil.TempDir("", "test")
	server.SetPath(temp)

	session := server.Session()
	if _, err := daos.InitSession(session); err != nil {
		panic(err)
	}
	db = session
}
