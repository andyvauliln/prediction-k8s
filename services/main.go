package services

import (
	"errors"

	"gitlab.com/andyvauliln/prediction-k8s/utils"
)

var logger = utils.Logger

var ErrMarketNotFound = errors.New("Market not found")

var ErrMarketExists = errors.New("Market Exists")

var ErrMarketInvalid = errors.New("Market Invalid")

var ErrEtokenNotFound = errors.New("Etoken not found")

var ErrEtokenExists = errors.New("Etoken Exists")

var ErrEtokenInvalid = errors.New("Etoken Invalid")
