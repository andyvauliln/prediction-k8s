package services

import (
	"github.com/ethereum/go-ethereum/common"
	"gitlab.com/andyvauliln/prediction-k8s/interfaces"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/andyvauliln/prediction-k8s/types"
)

// EtokenService struct with daos required, responsible for communicating with daos.
// EtokenService functions are responsible for interacting with daos and implements business logics.
type EtokenService struct {
	EtokenDao interfaces.EtokenDao
}

// NewEtokenService returns a new instance of balance service
func NewEtokenService(
	EtokenDao interfaces.EtokenDao,

) *EtokenService {

	return &EtokenService{EtokenDao}
}

// Create function is responsible for inserting new Etoken in DB.
// It checks for existence of Etoken in DB first
func (s *EtokenService) Create(Etoken *types.Etoken) error {
	p, err := s.GetByEtokenAddress(Etoken.Address)
	if err != nil {
		return err
	}

	if p != nil {
		return ErrEtokenExists
	}

	err = s.EtokenDao.Create(Etoken)
	if err != nil {
		return err
	}

	return nil
}

// GetByID fetches details of a Etoken using its mongo ID
func (s *EtokenService) GetByID(id bson.ObjectId) (*types.Etoken, error) {
	return s.EtokenDao.GetByID(id)
}

// GetAll is reponsible for fetching all the Etokens in the DB
func (s *EtokenService) GetAll() ([]types.Etoken, error) {
	return s.EtokenDao.GetAll()
}

// GetTrades is currently not implemented correctly
func (s *EtokenService) GetEtokens(bt, qt common.Address) ([]types.Etoken, error) {
	return s.EtokenDao.GetAll()
}

// GetByAddress fetches the detailed document of a token using its contract address
func (s *EtokenService) GetByEtokenAddress(Address common.Address) (*types.Etoken, error) {
	return s.EtokenDao.GetByEtokenAddress(Address)
}
