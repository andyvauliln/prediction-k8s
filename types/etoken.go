package types

import (
	"encoding/json"

	"github.com/ethereum/go-ethereum/common"
	validation "github.com/go-ozzo/ozzo-validation"
	"gopkg.in/mgo.v2/bson"
)

type Etoken struct {
	ID          bson.ObjectId  `json:"id" bson:"_id"`
	TokenID     uint           `json:"tokenid" bson:"id"`
	ImageURL    string         `json:"image_url,omitempty" bson:"image_url"`
	Name        string         `json:"name,omitempty"`
	Description string         `json:"des,omitempty" bson:"des"`
	Volume      string         `json:"volume,omitempty"`
	Price       string         `json:"price,omitempty"`
	Change      string         `json:"change,omitempty"`
	Address     common.Address `json:"address,omitempty"`
	MarketCap   string         `json:"market_cap,omitempty" bson:"market_cap"`
	IsEtherscan uint8          `json:"isEtherscan,omitempty"  bson:"isEtherscan"`
}

// TokenRecord is the struct which is stored in db
type EtokenRecord struct {
	ID          bson.ObjectId `json:"id" bson:"_id"`
	TokenID     uint          `json:"tokenid" bson:"id"`
	ImageURL    string        `json:"image_url,omitempty" bson:"image_url"`
	Name        string        `json:"name,omitempty"`
	Description string        `json:"des,omitempty" bson:"des"`
	Price       string        `json:"price,omitempty"`
	Address     string        `json:"address,omitempty"`
	Change      string        `json:"change,omitempty"`
	MarketCap   string        `json:"market_cap,omitempty" bson:"market_cap"`
	IsEtherscan uint8         `json:"isEtherscan,omitempty"  bson:"isEtherscan"`
	Volume      string        `json:"volume,omitempty"`
}

func (m *Etoken) SetBSON(raw bson.Raw) error {
	decoded := &EtokenRecord{}

	err := raw.Unmarshal(decoded)
	if err != nil {
		return err
	}

	m.ID = decoded.ID
	m.TokenID = decoded.TokenID
	m.ImageURL = decoded.ImageURL
	m.Name = decoded.Name
	m.Description = decoded.Description
	m.Volume = decoded.Volume
	m.MarketCap = decoded.MarketCap
	m.Price = decoded.Price
	m.Change = decoded.Change
	m.IsEtherscan = decoded.IsEtherscan
	if common.IsHexAddress(decoded.Address) {
		m.Address = common.HexToAddress(decoded.Address)
	}

	return nil
}

func (m *Etoken) GetBSON() (interface{}, error) {
	return &EtokenRecord{
		ID: m.ID,

		TokenID:     m.TokenID,
		ImageURL:    m.ImageURL,
		Name:        m.Name,
		Description: m.Description,
		Volume:      m.Volume,
		Price:       m.Price,
		Address:     m.Address.Hex(),
		Change:      m.Change,
		IsEtherscan: m.IsEtherscan,
		MarketCap:   m.MarketCap,
	}, nil
}

// Validate function is used to verify if an instance of
// struct satisfies all the conditions for a valid instance
func (m Etoken) Validate() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.TokenID, validation.Required),
	)
}

func (p *Etoken) Print() {
	b, err := json.MarshalIndent(p, "", "  ")
	if err != nil {
		logger.Error(err.Error())
	}

	logger.Info(string(b))
}
