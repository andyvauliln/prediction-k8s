package cmd

import (
	"fmt"
	"net/http"

	"github.com/ethereum/go-ethereum/log"
	"github.com/gorilla/mux"
	"github.com/mailru/dbr"
	_ "github.com/mailru/go-clickhouse"
	"github.com/spf13/cobra"
	"gitlab.com/andyvauliln/prediction-k8s/app"
	"gitlab.com/andyvauliln/prediction-k8s/daos"
	"gitlab.com/andyvauliln/prediction-k8s/endpoints"
	"gitlab.com/andyvauliln/prediction-k8s/ethereum"
	"gitlab.com/andyvauliln/prediction-k8s/services"
	"gitlab.com/andyvauliln/prediction-k8s/utils"
	"gitlab.com/andyvauliln/prediction-k8s/ws"
)

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Get application up and running",
	Long:  `Get application up and running`,
	Run:   run,
}

func init() {
	rootCmd.AddCommand(serveCmd)
}

func run(cmd *cobra.Command, args []string) {
	// connect to the database
	_, err := daos.InitSession(nil)
	if err != nil {
		panic(err)
	}

	provider := ethereum.NewWebsocketProvider()

	connect, err := dbr.Open("clickhouse", app.Config.ClickHouseUrl, nil)
	if err != nil {
		log.Error(err.Error())
	}

	db := connect.NewSession(nil)
	_ = db
	router := NewRouter(provider)
	http.Handle("/", router)
	http.HandleFunc("/socket", ws.ConnectionEndpoint)

	// start the server
	address := fmt.Sprintf(":%v", app.Config.ServerPort)
	log.Info("server %v is started at %v\n", app.Version, address)
	panic(http.ListenAndServe(address, nil))
}

func NewRouter(provider *ethereum.EthereumProvider) *mux.Router {

	r := mux.NewRouter()
	utils.Use(provider)
	// get daos for dependency injection
	marketDao := daos.NewMarketDao()
	etokenDao := daos.NewEtokenDao()

	// get services for injection
	marketService := services.NewMarketService(marketDao)
	etokenService := services.NewEtokenService(etokenDao)

	// deploy http and ws endpoints
	endpoints.ServeMarketResource(r, marketService)
	endpoints.ServeEtokenResource(r, etokenService)

	return r
}
