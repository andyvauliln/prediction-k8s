package main

import "gitlab.com/andyvauliln/prediction-k8s/cmd"

func main() {
	cmd.Execute()
}
