package utils

import (
	"math/big"

	"gitlab.com/andyvauliln/prediction-k8s/utils/math"
)

func Ethers(value int64) *big.Int {
	return math.Mul(big.NewInt(1e18), big.NewInt(value))
}
