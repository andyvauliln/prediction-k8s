package daos

import (
	"errors"

	"github.com/ethereum/go-ethereum/common"
	"gitlab.com/andyvauliln/prediction-k8s/app"
	"gitlab.com/andyvauliln/prediction-k8s/types"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Etoken  Dao contains:
// collectionName: MongoDB collection name
// dbName: name of mongodb to interact with
type EtokenDao struct {
	collectionName string
	dbName         string
}

// Etoken dao options
type EtokenDaoOption = func(*EtokenDao) error

// Etoken Dao pre-settings
func EtokenDaoDBOption(dbName string) func(dao *EtokenDao) error {
	return func(dao *EtokenDao) error {
		dao.dbName = dbName
		return nil
	}
}

// NewEtokenDao returns a new instance of AddressDao
func NewEtokenDao(options ...EtokenDaoOption) *EtokenDao {
	dao := &EtokenDao{}
	dao.collectionName = "tokens"
	dao.dbName = app.Config.DBName

	for _, op := range options {
		err := op(dao)
		if err != nil {
			panic(err)
		}
	}

	index := mgo.Index{
		Key:    []string{"id", "address"},
		Unique: true,
	}

	err := db.Session.DB(dao.dbName).C(dao.collectionName).EnsureIndex(index)
	if err != nil {
		panic(err)
	}

	return dao
}

// Create function performs the DB insertion task for Etoken collection
func (dao *EtokenDao) Create(Etoken *types.Etoken) error {
	Etoken.ID = bson.NewObjectId()
	Etoken.IsEtherscan = 0

	err := db.Create(dao.dbName, dao.collectionName, Etoken)
	return err
}

// GetAll function fetches all the Etokens in the Etoken collection of mongodb.
func (dao *EtokenDao) GetAll() ([]types.Etoken, error) {
	var response []types.Etoken
	err := db.Get(dao.dbName, dao.collectionName, bson.M{}, 0, 0, &response)
	return response, err
}

// GetByID function fetches details of a Etoken using Etoken's mongo ID.
func (dao *EtokenDao) GetByID(id bson.ObjectId) (*types.Etoken, error) {
	var response *types.Etoken
	err := db.GetByID(dao.dbName, dao.collectionName, id, &response)
	return response, err
}

// GetByTokenAddress function fetches Etoken based on
// CONTRACT ADDRESS of base token and quote token
func (dao *EtokenDao) GetByEtokenAddress(Address common.Address) (*types.Etoken, error) {
	var res []*types.Etoken

	q := bson.M{
		"address": Address.Hex(),
	}

	err := db.Get(dao.dbName, dao.collectionName, q, 0, 1, &res)
	if err != nil {
		return nil, err
	}

	if len(res) == 0 {
		return nil, errors.New("Etoken not found")
	}

	return res[0], nil
}
