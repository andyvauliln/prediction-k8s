# ARG GOLANG_VERSION=1.11
# FROM golang:${GOLANG_VERSION} as golang-build
# ARG username=andyvauliln
# ARG repo_name=prediction-k8s
# COPY . /go/src/gitlab.com/${username}/${repo_name}
# WORKDIR /go/src/gitlab.com/${username}/${repo_name}
# RUN go get -u github.com/golang/dep/cmd/dep
# RUN dep ensure
# ENV CGO_ENABLED=1
# ENV GOOS=linux
# ENV GOARCH=amd64
# RUN go build -a -v --ldflags '-v -linkmode external' -tags netgo -installsuffix netgo -o /program .
# ENTRYPOINT ["/program", "serve"] 

# EXPOSE 8080 


# # STEP 1 build executable binary
# FROM golang:alpine as builder
# # Install SSL ca certificates
# RUN apk update && apk add git && apk add ca-certificates
# RUN go get github.com/golang/dep/cmd/dep
# # Create appuser
# RUN adduser -D -g '' appuser
# ARG username=andyvauliln
# ARG repo_name=prediction-k8s
# COPY . /go/src/gitlab.com/${username}/${repo_name}
# WORKDIR /go/src/gitlab.com/${username}/${repo_name}
# #get dependancies
# RUN dep ensure -vendor-only
# #build the binary
# RUN go build -a -v
# # STEP 2 build a small image
# # start from scratch
# FROM scratch
# COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
# COPY --from=builder /etc/passwd /etc/passwd
# # Copy our static executable
# COPY --from=builder /go/bin/program /go/bin/program
# USER appuser

# EXPOSE 8080
# ENTRYPOINT ["/go/bin/program"]


# FROM scratch
# ARG username=andyvauliln
# ARG repo_name=prediction-k8s
# COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
# COPY --from=builder /etc/passwd /etc/passwd
# # Copy our static executable
# COPY --from=builder /go/src/gitlab.com/${username}/${repo_name} /go/src/gitlab.com/${username}/${repo_name}
# USER appuser

# EXPOSE 8080
# CMD ["go", "run", "main.go"]


# FROM jamesandariese/go-darwin-amd64:1.4.2
# RUN mkdir -p /wrkd
# WORKDIR /wrkd
# COPY ./program4 /wrkd/program4 
# EXPOSE 8080
# RUN go env
# RUN pwd
# CMD  go env


ARG GOLANG_VERSION=1.11
FROM golang:${GOLANG_VERSION} as golang-build
ARG username=andyvauliln
ARG repo_name=prediction-k8s
COPY . /go/src/gitlab.com/${username}/${repo_name}
WORKDIR /go/src/gitlab.com/${username}/${repo_name}
RUN go get -u github.com/golang/dep/cmd/dep
RUN dep ensure
ENV CGO_ENABLED=1
ENV GOOS=linux
ENV GOARCH=amd64
RUN go build -a -v --ldflags '-v -linkmode external' -tags netgo -installsuffix netgo -o /program .
EXPOSE 8080
ENTRYPOINT ["/program", "serve"] 